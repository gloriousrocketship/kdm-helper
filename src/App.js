import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import * as pages from './pages';
import './App.css';

import Header from './components/Header';

class App extends Component {
  render() {
    return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <Switch>
          <Route exact={true} path="/" component={pages.HuntEvents}/>
          <Route exact={true} path="/hunt-events" component={pages.HuntEvents}/>
        </Switch>
      </div>
    </BrowserRouter>
    );
  }
}

export default App;
