import * as React from 'react';
import * as ReactMarkdown from 'react-markdown';
import HuntEventList from '../data/hunt-events';
import './HuntEvents.css'

class HuntEvents extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        nothingEnteredYet: true,
        numberText: '',
        eventResult: undefined,
      }
      this.numberTextDidChange = this.numberTextDidChange.bind(this)
      this.updateEventResult = this.updateEventResult.bind(this)
      this.buildEventError = this.buildEventError.bind(this);
      this.buildEventResult = this.buildEventResult.bind(this);
  }

  componentDidMount() {
    // reset state
    this.setState({nothingEnteredYet: true, eventResult: undefined, numberText: ''});
    // this.setState({numberText: '1'}, this.updateEventResult);
  }

  numberTextDidChange(event) {
    this.setState({nothingEnteredYet: false, numberText: event.target.value}, this.updateEventResult);
  }

  updateEventResult() {
    // show event or an error
    let { eventResult, numberText } = this.state;
    let eventNumber = parseInt(numberText, 10);
    eventResult = undefined;
    if (typeof eventNumber === 'number') {
      try {
        // WARN: brittle if the list is ever out of order, change to find if needed
        eventResult = HuntEventList.find((huntEv) => { return huntEv.huntEventNumber  === `${eventNumber}`; });
      } catch (error) {
        eventResult = undefined;
      }
      this.setState({eventResult})
    }
  }

  buildEventError() {
    // if event result not found
    if (this.state.numberText && this.state.numberText.length > 0 && this.state.eventResult === undefined) {
      return (<div className="error-message">(enter between 1 - 100)</div>);
    }
    return (<div className="error-message empty">&nbsp;</div>);
  }

  buildEventResult() {
    if (this.state.numberText && this.state.eventResult) {
        let { huntEventNumber, title, descriptionMk } = this.state.eventResult;
        return (
        <div className={'event-info ' + huntEventNumber}>
          <div className="title">{title}</div>
          <div className="description markdown-wrapper"><ReactMarkdown source={descriptionMk}/></div>
        </div>);
    }
    return (<div className="event-info empty">{this.state.nothingEnteredYet ? 'Bad news. Faster.' : ''} <br/><br/> KD:M v1.5 only</div>);
  }

  render() {
    return (
      <div className="Page HuntEvents">
        <div className="title">Hunt Event Lookup</div>
        <div className="form">
            {this.buildEventError()}
            <input type="number" placeholder={'↵ a #'} value={this.state.numberText} onChange={this.numberTextDidChange}/>
        </div>
        <div className="event-info-wrapper">
            {this.buildEventResult()}
        </div>
      </div>
    );
  }
}

export default HuntEvents;
