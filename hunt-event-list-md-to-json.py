"""Use Python 3.6 (f-strings)."""

import re
import json
import time
# This file opens HuntEventList.md and scans for events, puts all found events into a tempfile.
start_millis = int(round(time.time() * 1000))
source_markdown_f = 'HuntEventsList.md'
hunt_event_rx = re.compile('([\d]+) - ([a-zA-Z \-\'!]+) :\n(([^#])*)\n# ')
# capture group 1: hunt event number
# capture group 2: hunt event name
# capture group 3: hunt event description (markdown)


def hunt_event_tuple_to_object(t):
    """Change groups of a single regex match to a object for json."""
    to_ret = {'huntEventNumber': t[0], 'title': t[1], 'descriptionMk': t[2]}
    return to_ret


events_list = []  # list of valid hunt event objects (to become json)

with open(source_markdown_f) as f:
    entire_file = f.read()
    matches = hunt_event_rx.findall(entire_file)
    for m in matches:
        events_list.append(hunt_event_tuple_to_object(m))

millis = int(round(time.time() * 1000))  # nearest integral number
out_f = f'temp_hunt_list-{millis}.json'
with open(out_f, 'w') as outfile:
    json.dump(events_list, outfile)

print(f'printed {len(events_list)} found hunt events to {out_f} ({millis - start_millis} millis)')
