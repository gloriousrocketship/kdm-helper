# 1 - Broken Lanterns :

The survivors hear the crunching of glass beneath their feet.

The event revealer may choose to investigate or continue their journey.

If they choose to investigate, the event revealer gains +1 courage and rolls on the table below. Otherwise roll again on the hunt event table before moving on the hunt event board.

| 1 d10 | Event revealer only - Investigate |
| --- | ---- |
| 1 - 3 | The event revealer fumbles and cuts their foot on a jagged shard. Taking Monster Level event damage to their legs. |
| 4 - 8 | The event revealer finds a broken lantern basic resource. |
| 9 + | The survivors uncover a long-abandoned lantern hoard. The forlorn sight fills them with dread and each survivor sets their insanity to 0. After composing themselves. Each survivor scavenges 1 broken lantern basic resource. Add them to settlement storage. |

# 2 - Corpse :

The survivors are overcome by a sudden chill, their breath seizing in their lungs. The survivors freeze, each losing Monster Level survival. At the center of the sudden frost is a perfectly preserved corpse.

If your settlement chose the Cannibalize death principle, the event revealer gains 1 random basic resource.

If your settlement the Graves death principle, the event revealer gains +1 courage and +1 understanding.

If your settlement has Memento Mori, the event revealer understands a little about what happened to the corpse, they gain 1 random fighting art.


# 3 - Cancer Pigeons :

The survivors are surrounded by an echoing coo of infant babble. Strange baby-faced birds circle overhead. Gripped with instinctual horror, the survivors break into a run!

 Every survivor must roll 1d10. The lowest scoring survivor (or survivors, in case of ties) becomes a straggler.
 If any survivor has noisy gear, -2 to the roll result.

 Each straggler rolls on the table.

 | 1d10 | Straggler |
 | - | - |
 | 1 - 2 | Running and flailing wildly. You suddenly find yourself panicked and alone. The waiting cancer pigeons descend and mercilessly peck your back. Their happy cooing filling your head with horror. The other survivors find you bailed up and weeping on the ground. You are *Dead Inside*: You cannot gain survival. Record this impairment. |
 | 3 - 4 | A cancer pigeon latches onto your back! Shake it free by spending 1 survival. Otherwise, are *Dead Inside*: You cannot gain survival. Record this impairment. |
 | 5 - 6 | You escape the terrible creatures. |
 | 7 + | You manage to strike one of the foul creatures down. Gain 1 random basic resource. |

# 4 - Famine :

The path ahead is arid and dead. You may have survivors gain +1 courage to press on. Otherwise, roll again on the hunt event table before moving on the hunt event board.

If you press on, your stomachs grumble in the dark as you stumble forward. The survivors pool their supplies. The hunt revealer rolls on the table, adding +2 to their roll for each consumable gear or resource the group has (do not archive this gear).

 | 1d10 | Event Revealer |
 | - | - |
 | 1 - 8 | The hunt takes its toll. Powerful hunger pangs. All survivors gain -1 speed token. |
 | 9 - 14 | You manage to stave off the terrible hunger |
 | 15+ | Everyone is invigorated by their snack. Each survivor gains +1 survival. |


# 5 - Flesh Fields :

Ahead, the stone faces on the ground give way to a field of man-sized fleshy boils sprouting thick black hair. The fleshy fields radiate moist heat. As they travel, every survivor suffers heat exhaustion, losing 1 survival.

The event revealer rolls on the table. If any survivor has a sickle, they help clear a path through the thick black hair, adding +4 to your roll.

| 1d10 | Event revealer |
| - | - |
| 1 | The event revealer contracts a flesh-eating disease brushing past the giant boils! A random limb is affected and must be amputated immediately! Roll 1d10, 1-5: arm. 6-10: leg. Suffer the dismembered severe injury at this location.
| 2 - 3 | If any survivor is insane, they imagine the flesh fields are attacking! They become hysterical, flailing and tangling themselves in the thick hair. Each insane survivor suffers Monster Level event damage to a random location. |
| 4 - 7 | It's hot and gross. but the survivors pass through the flesh fields with no complications. |
| 8 - 9 | The event revealer makes the best of a bad situation and cuts a portion of the flesh free. Gain 1 hide basic resource. |
| 10+ | Each survivor harvests 1 hide basic resource from the flesh fields. |


# 6 - Faceless Statue :
The survivors come upon a faceless statue holding a shining lantern in hand. Basking in the warm light, the survivors are renewed.

Each survivor gains 1 survival.

If the survivors wish, they may rest here longer. If they do, each survivors heal all injury levels and lost armor points on 1 hit location. However, they lose track of their quarry. Move the quarry 1 space away from the survivors on the hunt board.

If a survivor heals more than 5 lost armor hit points this way, they stare into the shining light for too long and gain 1 random disorder.

If all survivors are insane, they cannot rest longer.

# 7 - Hungry Ground :
The ground suddenly splits and a gaping stone mouth attempts to devour the event revealer whole! The event revealer rolls on the table.

If another survivor has a whip, they throw a line to the event revealer. Add +4 to your roll.

| 1d10 | Event Revealer |
| - | - |
| 1 | With a muffled scream, you disappear into the ground. You manage to grab ahold of a ledge above the gaping maw. Lose all survival as you scramble up to safety. If you had no survival, you fall. Bones crunch between stone teeth. The event revealer is devoured whole. The event revealer is dead. |
| 2 - 5 | The event revealer scrambles but their leg is caught in the terrible maw. Suffer 2 event damage to the legs. |
| 6 - 9 | The survivor manages to leap away in time! |
| 10+ | Burp! Gurgle! The floor vomits a shiny piece of scrap. The event revealer gains 1 broken lantern basic resource. |

# 8 - Wailing Smoke :

A constant moan follows the survivors. Eventually, they enter an area shrouded in thick fog and the wailing reaches a fevered pitch.

Every non-deaf survivor must roll 1d10. If all survivors are deaf, they continue along, heedless to the cacophony (end this event).

The lowest scoring survivor (or survivors, in case of ties) becomes a straggler. Each straggler rolls on the table. If any survivor has noisy gear, the straggler adds +2 to their roll.

| 1d10 | Straggler |
| - | - |
| 1 - 3 | The fog around the straggler turns black and fills with shapes of tormented faces. The straggler is driven to madness and gains a random disorder. |
| 4 - 6 | The fog grows soupy and begins to wail. The straggler plugs their ears with their fingers, but it's hopeless. Suffer Monster Level brain event damage. |
| 7 - 9 | The straggler sticks their thumbs into their ears and breaks into a run, leaving the wailing smoke. |
| 10+ | Mimicking the disturbing smoke, the straggler starts to wail and gains +2 insanity.

# 9 - Golden Light :

A golden light blinds the survivors, stopping them in their tracks. The light shines from a single source. The event revealer must investigate, they gain +1 courage then roll on the table.

If the event revealer is blind, they walk in the wrong direction and gain +3 insanity. Roll again on the hunt table and do not investigate the light.

| 1d10 | Event revealer - Investigate |
| - | - |
| 1 - 3 | The light is overpowering. The fragile mind cannot understand what it is saying. Gain 1 random disorder. |
| 4 - 8 | You wander through the light, bathed in a warm, awful feeling. You don‘t find anything other than exposure. Suffer Monster Level event damage to a random hit location. |
| 9+ | Something about the light stirs you, and your resolve intensifies. Gain +1 survival. |

# 10 - Harvester :

This event cannot be rerolled or avoided in any way.

The ground quakes violently as a colossal worm bursts from the ground beneath the survivors, its skin a mosaic of screaming faces. The mere sight of it is maddening.

All survivors gain 1 random disorder and must spend 1 survival or be devoured whole. If any survivor has any noisy gear, the Harvester is drawn to the sound and they are instantly devoured. Nothing can save them.

The creature is so horrifying that the survivor's brains erase the experience from their memories. Do not gain the benefits of your death principle.


# 11 - Monster Droppings :
The survivors find some monster droppings, like those of their quarry. The event revealer must choose to either investigate or consume the droppings (choose only one).

If the event revealer investigates, roll on the table and gain +1 understanding.

| 1d10 | Event revealer - Investigate |
| - | - |
| 1 - 3 | You discover just how sickening playing with feces is. Suffer -1 survival and roll again on the hunt event table before moving on the hunt board. |
| 4 - 6 | The droppings tell a story. The survivors are on the right track. Gain +1 survival. |
| 7+ | You know these droppings well. You may skip the next hunt space. If this move begins the showdown, the survivors ambush their quarry. |

If the event revealer consumes, roll on the table and gain +1 courage.

| 1d10 | Event Revealer - Consume |
| - | - |
| 1 - 3 | It seems you weren't the only one with this idea! You attempt to cough out the refuse but your mouth is full of wriggling parasites! They burrow upwards through your nasal cavity into your brain. From now on, you always feel slightly dizzy. Suffer -1 permanent evasion. In addition, you have the Quixotic disorder. The parasites savor the unique neural activity of your brain, feeding relentlessly and excreting waste which further fuels your madness. Gain +10 insanity and the Immortal Disorder. Your Quixotic disorder can never be removed for any reason. |
| 4 - 6 | It‘s repulsive. Replace your name with 'Scat Man.' Everyone in the settlement calls you by that name from now on. |
| 7+ | There was something quite special about these feces. Gain +1 speed and +1 strength token. |

# 12 - Destiny-Bound :

The survivors collectively feel a heightened level of anticipation. Their goal is close at hand.

The event revealer gains +1 courage! The survivors may choose to skip the next hunt space. If this movement starts the showdown, the survivors ambush the monster.

# 13 - Overload :

The event revealer falls to their knees sobbing uncontrollably. They've simply had too much. They lose 1d10 survival and gain 1 random disorder.

If the settlement has Accept Darkness, the event revealer embraces their despair. Roll on the table below.

| 1d10 | Event Revealer |
| - | - |
| 1 - 5 | The depths of your misery are deeper than you imagined. Gain disorders until you have 3 of them. Lose all survival. |
| 6 + | You sink to the bottom of your misery and then scrape the foundations, hungry for more! You uncover a primal rage surrounding a core of serene calm. Gain +1 understanding and the Rageholic disorder. |

If the settlement has Collective Toil, they slowly pull themselves to their feet, promising to never give up hope. Roll on the table below.

| 1d10 | Event Revealer |
| - | - |
| 1 - 5 | You rise, silent tears streaming down your face. With the settlement at your back you are ready to face the darkness. Gain +1 courage and +1d10 survival. |
| 6 + | You briefly touch the bottom of your misery return to full awareness with new experiences to share. If any survivors return to the settlement this year, gain +2 endeavors. |


# 14 - Chance Encounter :

The survivors encounter a bewitching, barefoot waif.

If you already rolled this event on this hunt, or if all survivors are insane, she turns her back and disappears into the darkness. Roll again on the hunt event table before moving.

Otherwise, she exclaims wildly at the survivors. The woman's presence fills everyone with the alien feeling of hope. All survivors gain +1 courage and +1 understanding.

If the settlement has a Hovel, she snaps her fingers and ages before their eyes! A chill passes over the survivors. Their bodies and armor spontaneously knit themselves together. Heal all injury levels and lost armor points.

Then, she pulls a random survivor aside and whispers secrets of the upcoming hunt. They gain +1 accuracy token.

# 15 - Man-trap :

The event revealer has sprung a terrible trap! Roll on the table.

| 1d10 | Event revealer |
| - | - |
| 1 - 2 | With a terrible crash, the steel jaws of the man-trap cleanly sever your foot. Suffer the dismembered leg severe injury. |
| 3 - 6 | Mangled leg. Reduce armor points at the legs location to 0 and suffer 1 event damage to the legs. |
| 7 - 9 | The trap is quite old and weak, suffer 1 event damage to the legs. |
| 10+ | The event revealer's foot knocks into the trap, giving everyone a scare! The trap was already sprung, leaving an unfortunate prize in its jaws. Gain 1 random basic resource. |

# 16 - Night Terrors :
Your miserable sleep is plagued with mind-bending nightmares.

All survivors must roll 1d10. If the result is greater than their insanity, they learn something from the waking horror and gain +1 understanding. Otherwise, they gain 1 random disorder.

If any survivor is a savior or has the Extra Sense fighting art, they disappear into the dream, emerging from the darkness hours later. They gain +4 Hunt XP and a random basic resource.

# 17 - Face-to-Face :

One stone face in the sea of many catches the event revealer's eye.

The event revealer must investigate. Gain +1 courage and roll on the table.

| 1d10 | Event revealer - Investigate |
| - | - |
| 1 |  The face is identical to your own. Spend 1d5 survival or reduce your understanding to 0. |
| 2 - 7 | The face reminds you of someone dear, your heart aches with loneliness. If your survival is higher than your insanity, suffer Monster Level brain event damage. |
| 8+ | The face is your own, but older and wiser. Gain +1 survival and +1 understanding. |

# 18 - Dead Weed :

You find a strange plant growing from the eyes of a stone face. The event revealer tries to harvest it by rolling on the table below. If any survivor has the Bone Sickle, add +4 to the result.

| 1d10 | Event revealer |
| - | - |
| 1 - 7 | Try as you might, you cannot pull out the deeply-rooted weed. |
| 8+ | You pull the weed free! It has strange regenerative powers. Each survivor rubs it on their skin and removes 1 permanent severe injury. It crumples into dust in your hands when you move on. |

# 19 - Exhalation of Darkness :

The survivors are enveloped in a steamy darkness that dims their lantern lights. They become separated from each other and hopelessly lost.

If a survivor has the Leader fighting art, they boldly stride into the the clammy darkness and gather everyone. Otherwise, each survivor rolls 1d10. The lowest scoring survivor (or survivors, in the case of a tie) becomes a straggler. Each straggler rolls on the table below.

| 1d10 | Straggler |
| - | - |
| 1 - 2 | You trip and all, smashing a piece of gear. Archive a gear card of your choice from your gear grid. |
| 3 - 5 | Something unseen attacks you, suffer 1 brain event damage and 1 event damage to a random location. |
| 6 - 8 | You're struck from behind by a flailing survivor, suffer 1 event damage to the head. |
| 9+ | You're not sure how, but you leave the darkness with your hands full, gain 1 random basic resource. |

# 20 - Dry Lands :

As the survivors press forward, the air turns dry and the ground cracks. Each survivor suffers from heat exhaustion.

Each survivor loses 1 survival. If they are wearing fur or heavy gear, they lose 1d5 survival instead.

If any survivor has a pickaxe, the survivors break open he an interesting looking vein in the cracked ground. Gain 1 Iron strange resource.

# 21 - Drawings :
Cowering in a cave, the survivors find strange drawings decorating the walls. They appear to carry a message.

Any survivor with 3+ understanding may try to decipher the drawings by rolling on the table.

If your settlement has innovated Pictographs, every survivor may roll (regardless of their understanding) and add +4 to their rolls.


| 1d10 | Any survivor with 3+ understanding |
| - | - |
| 1 - 3 | Your nose begins to bleed. Suffer Monster Level brain event damage. |
| 4 - 7 | The true meaning escapes you. Gain +1 insanity. |
| 8+ | The drawings teach you a secret way to fight monsters! Gain +2 survival and a random Fighting Art.

# 22 - Acid Rain :
The darkness above is suddenly alight with a storm of acid rain. It smells terrible, burns flesh, and begins to form hissing pools in the upturned mouths of the stone faces.

All survivors suffer Monster Level event damage to every hit location as they scramble to find shelter.

If the settlement has Sculpture, the survivors silently watch the storm erode the stone faces erasing their features. They wonder exactly who or what repairs the broken faces. The survivors share a dark laugh, each gaining Monster Level Survival.

Event revealer rolls 1d10. On a roll of 6+, the storm moves homeward. Add the Acid Storm settlement event to the timeline next year.

# 23 - Clean Water :
The survivors pass a forlorn fountain in the shape of a lion face. A stream of crystal clear water flows from its mouth.

Each survivor consumes the water, gaining +1 courage and +1 survival.

In addition, each survivor may douse their head in the water and roll on the table.

| 1d10 | Any survivor |
| - | - |
| 1 - 2 | Your hair turns white the instant it is wet. Gain 1 random disorder. |
| 3 - 5 | Your head is wet. Nothing else happens. |
| 5 - 8 | The water is bracing. Gain +1 understanding or remove a disorder of your choice. |
| 9+ | The water is shockingly cold - it rushes over you like a tidal wave and leaves you awestruck. Set your insanity to 0 and remove all disorders. |

# 24 - Food from the Mouths of Others :
The survivors pass a stone face that appears to be holding something in its mouth.

The event revealer investigates. Gain 1 courage and roll on the table.

| 1d10 | Event revealer - Investigate |
| - | - |
| 1 - 2 | You yell out in pain and pull your hand back, revealing a bloody gash. Did the stone face just bite? Suffer Monster Level brain event damage and Monster Level event arm damage. |
| 3 - 5 | There's nothing. Just a trick of the lantern light. |
| 6 - 8 | More rocks, what did you expect? Gain 1 Founding Stone gear. |
| 9+ | You pull out a masticated mass. Gain 1 random basic resource and 2 insanity. |

# 25 - Titans in the Dark :
An ear-shattering bellow marks the passing of a truly titanic beast.

All non-deaf survivors suffer Monster Level brain event damage. The survivors cower on the ground, lying as flat as possible and hoping they are simply too small to be worth eating.

Every survivor must roll 1d10. The lowest scoring survivor (or survivor, in case of a tie) becomes a straggler. If any survivor has noisy gear, -2 to the roll result.

Each straggler rolls on the table below.

| 1d10 | Straggler |
| - | - |
| 1 | You're plucked from the ground and eaten whole. Dead. However, if the settlement has Survival of the Fittest, you twist out of its mouth and land with a thud. Suffer the broken arm and broken leg severe injuries, but do not die. |
| 2 - 4 | Something snatches you in its great maw and chews you for 1 event damage to 1d5 random hit locations. |
| 5 - 6 | The clamping jaws barely nip you! Suffer Monster Level event damage to a random hit location. |
| 7 - 9 | You make yourself extra flat against the ground and avoid anything terrible. |
| 10+ | You will never stop running from what you saw. Gain +1 permanent movement. |

# 26 - Pit :
The ground beneath the event revealer‘s feet gives way, and they plummet into a pit below. The event revealer rolls on the table.

If another survivor has a whip, they throw you a line in time; do not roll on the table.

If the event revealer has the Tumble fighting art, they nimbly catch a ledge and haul themselves up. Gain +1 speed token and do not roll on the table.

| 1d10 | Event revealer |
| - | - |
| 1 | The fall mangles your legs. Suffer the broken leg severe injury twice. |
| 2 - 3 | Your leg breaks your fall. Suffer the broken leg severe injury.
| 4 - 6 | You land at the bottom with a loud thud. Suffer 2 event damage to a random hit location and archive any fragile gear in your gear grid. |
| 7 - 9 | You land at the bottom with a soft thud. Suffer 1 event damage to a random hit location. |
| 10+ | You manage to grab the edge of the pit and escape unharmed. |

# 27 - Man-Stealers :

Exhausted, the survivors rest. During their fitful sleep. A small insect creature approaches undetected.

Choose 1 random male survivor and roll on the table. If there are no male survivors, nothing happens.

| 1d10 | Random male survivor |
| - | - |
| 1 - 5 | When you awake, your genitals have been stolen. In their place is nothing but smooth skin. Suffer the destroyed genitals severe injury. |
| 6+ | You awake with a start, terrified to find menacing pincers approaching. Your defenses are quick. Without thinking, you wallop the insect crawling on your pants, suffering 1 event damage to the waist. |

# 28 - Trollbird :
A rumpled, unsightly bird stands in the survivors‘ path. Its beady, wet eyes blink expectantly and it calls out with an eerie human chuckle.

The survivors may archive 1 consumable item or gear, offering it to the trollbird. If any survivor is insane, they must feed the trollbird if they have anything consumable.
If they feed the trollbird, it hops off with a terrible cackle.

If they don‘t feed the trollbird, it follows the survivors on their hunt, constantly mocking them with its chuckle. Roll on the table.

If any survivor has the Coprolalia disorder, they curse at the bird and the make vigorous and vulgar gestures. The bird is impressed, add +3 to your roll.

| 1d10 | Event revealer |
| - | - |
| 1 - 5 | The trollbird makes a terrible racket, alerting the monster.  All survivors gain +1 understanding. The monster ambushes the survivors at the start of the showdown. |
| 6+ | The trollbird mocks and distracts the monster as the survivors sneak up. All survivors gain +1 insanity. At the start of the showdown, the survivors ambush the monster.  |

# 29 - Dark Blacksmith :
The survivors cross paths with a tall, unnaturally thin man in a hooded robe. Where his feet strike the ground, sparks illuminate his path.

Each survivor with 3+ courage may give him one piece of gear and roll on the table. If they have the Honorable disorder, add +3 to their roll.

| 1d10 | Any survivor with 3+ courage |
| - | - |
| 1 - 2 | The creature raises the object to its mouth and eats it. It's gone. Archive the gear card. |
| 3 - 6 | The creature refuses the gear and stares at you with a single large eye. Suffer 1d10 brain event damage. |
| 7 - 8 | The creature unhinges its jaw and eats the piece of gear. In return. it hands you a shining shield. Gain the Steel Shield rare gear. |
| 9+ | The creature unhinges its jaw and eats the piece of gear. In return, it hands you a shining sword. Gain the Steel Sword rare gear.

# 30 - Rotten Faces :
The stone faces start to give under the survivors‘ feet. The faces are bloated and smell terrible. Each survivor must roll on the table.

| 1d10 | Each survivor |
| - | - |
| 1 | Your foot gets stuck and you quickly begin to sink into the ground. After a struggle you manage to break free. If your survivor had leg armor, it is lost in the muck. Spend 3 survival or archive the gear. |
| 2 - 5 | Mired, you struggle to break free! Roll again on this table or spend 1 survival to escape. |
| 6 - 9 | You escape without incident. |
| 10+ | You find something gross drudging through the rot! Gain 1 random basic resource. |

# 31 - Strange Path :
The survivors stop at the head of a path. Small lanterns twinkle, marking its edges.

The event revealer decides if the survivors follow the path. If the event revealer is insane, they must follow the path.

If the survivors follow the path, the event revealer gains +1 understanding, then rolls on the table below, adding their understanding to the result.

Otherwise, roll again on the hunt event table before moving on the hunt board.

| 1d10 | Event revealer |
| - | - |
| 1 | A colossal finger descends from above, crushing a random survivor. They suffer 2 event damage to every hit location. |
| 2 - 4 | A random survivor tears off hysterically along a branch in the path. They rejoin the other survivors, ghostly pale and with no recollection of what happened. They lose 1d10 survival. |
| 5 - 7 | The path twists and turns dizzyingly into the dark. All survivors suffer 1 brain event damage. The event revealer rolls again on this table, adding +2 to their roll. |
| 8 - 9 | The path leads the survivors to a large stone face with lanterns for eyes. Inside its open mouth is a bounty. Each survivor gains 1 random basic resource. |
| 10+ | The path leads to the beast you are hunting! The survivors ambush the monster. If any survivor has the Honorable disorder, they realize the folly of their ways and may remove the disorder. If any survivors remain Honorable, they announce their presence to the monster; the monster ambushes the survivors instead.  

# 32 - Sudden Madness  :
The event revealer is devastated by a piercing cacophony in their head.

Roll 1d10. If the result is less than or equal to the revealer‘s current insanity, they go mad, lashing out at the other survivors! All other survivors suffer 1 event damage to a random hit location. After the damage is dealt, the event revealer regains control and everyone warily moves on.

If the event revealer has the Crazed fighting art, they gain insight from their ordeal. Gain +10 insanity and +2 understanding.

# 33 - It Whispers Your Name :
The event revealer trips over a protruding nose in the ground. The lips of a stone face begin to move!

If the event revealer has no name, the lips stop moving and nothing else happens. Otherwise, the lips seductively whisper the survivor's name.

If the event revealer is insane, they are drawn in, savagely kissing the face. Repulsed and outraged, the other survivors suffer 1 brain event damage before pulling the crazed survivor from the ground and moving on.

If the event revealer is not insane, they gain +1 understanding, feeling on the cusp of learning something.

If any survivor has the Marrow Hunger impairment, the afce secretly whispers their life's purpose. They gain +1 permanent strength. Add the Murder settlement event to the timeline next year.

# 34 - Crippling Misery :
Every survivor must roll 1d10. The lowest-scoring survivor (or survivors, in case of ties) becomes a straggler. If any survivor (or survivors) has the Anxiety disorder, they are the straggler. Do not roll.  

The straggler is lost in a moment of profound self doubt. They slip, and tumble off a steep hill of stone faces. They land awkwardly with a crunch, suffering the broken leg severe injury. Sobbing to themselves, they come to grips with just how terrible their life is and gain +1 understanding.

If the straggler has a Binge Eating Disorder, they wipe away their tears and reach out to a nearby critter, cramming it into their mouth for comfort. Draw 1 random vermin resource and immediately consume it.


# 35 - Broken Lantern Oven :
The survivors come across the shattered ruins of a small settlement. Scattered corpses dot the ruins, disintegrating at the merest touch. An extinguished lantern oven stands intact at the center of the settlement.

The event revealer rolls 1d10 and adds their understanding. If the result is a 5+, the survivors‘ settlement gains the Lantern Oven innovation. If they already have this innovation, the event revealer harvests a broken lantern basic resource from the remains.

If the settlement has Song of the Brave, the group may nominate a brave survivor to investigate the ruins. The nominated survivor gains +1 courage and rolls on the table.

| 1d10 | Nominated Survivor |
| - | - |
| 1 - 5 | You try to enter the ruined lantern hoard, but you are overcome with fear, keeling over and vomiting. When you rise, you find yourself physically incapable of pushing forward anymore. Gain the Apathetic disorder. |
| 6+ | You are repelled by the ruined lantern hoard, but linger just long enough to grab a pair of insects scurrying within it. Gain 2 vermin resources of your choice. |


# 36 - On the Trail :
The survivors rush forward, feeling their quarry close at hand.

Skip the next hunt space. If this movement begins the showdown, the survivors ambush their quarry. If any survivor has noisy gear, the survivors do not ambush. Start the showdown normally.

# 37 - Lost :

The survivors are hopelessly lost in the unbroken darkness.

Move the survivors 2 spaces away from their quarry.

# 38 - Gregalope :
A massive Antelope stands astride the horizon, its ancient body bloated with tumors and scar tissue. Its milky eyes catch sight of the survivors and it bounds away. Driven by a sudden desire, the survivors give chase.

The event revealer rolls 1d10 and adds their movement to the result

If any survivor has the Strategist fighting art, they may may corner the Gregalope at a giant stone face instead of giving chase. If they do, end the hunt immediately; Start the showdown with a Level 2 Screaming Antelope, using its setup rules. In addition, place a Giant Stone Face adjacent to the monster and give the monster the Butcher's Berserker trait card. If the survivors prevail, gain the normal showdown rewards and 3 additional Screaming Antelope resources.

| 1d10 | Event revealer |
| - | - |
| 1 - 6 | The survivors quickly fall behind the majestic beast. Move 1 space away from the quarry on the hunt board. |
| 7 - 14 | The Gregalope is far too fast. It is gone before you know it. |
| 15 - 16 | The survivors lose sight of the Gregalope but discover a hidden path. You may re-roll the next result on the hunt event table. |
| 17+ | Just before it leaps out of view, the Gregalope bows its mighty horns. The event revealer gains +1 permanent movement.

# 39 - Heavy Mist :
A heavy mist envelops the survivors, obscuring their lantern light. Roll 1d10.

If the result is even, the survivors stumble in the right direction. Next hunt turn, move normally on the hunt board.

If the result is odd, the survivors are turned around. Roll again on the hunt event table before moving on the hunt board.

If the showdown begins in the hunt board space following Heavy Mist, the monster ambushes the survivors.


# 40 - Dream :
The event revealer has a dream of the upcoming hunt. They stand before the great beast, weapon ready, but it strikes them down.

If the event revealer has 3+ courage, or if any survivor is a savior, the vent revealer is emboldened by the dream and gains +1 evasion token. Otherwise, the dream is merely rattling, suffer 1 brain event damage.

If any survivor has the Twilight Sword, they dream of indiscriminate slaughter. Add the Murder settlement event to the timeline 1d5 years from now.


# 41 - Nightmare :
The event revealer has a dream of the upcoming hunt. The great beast vanishes during the battle and follows them back to the settlement. They helplessly watch as the beast devours all they know.

The event revealer gains 1d10 insanity and -1 evasion token.

If the settlement has a savior, they appear in the dream and defend their home. The event revealer gains 1d5 survival.

If no survivor in the settlement or hunting party has a Twilight Sword, the event revealer envisions a maniacal version of themselves wilding the blade and slaughtering the beast. They awake to find the Twilight Sword at their side. Instinctively the survivor recognizes the weight of the weapon's curse and promise of its power. The event revealer gains the twilight sword rare gear. They may also select Twilight Sword as their weapon proficiency type and gain +1 weapon proficiency  with this weapon.


# 42 - Surgeon :
A creaky carriage approaches the survivors. Richly appointed in red and gold, the carriage is carved on every side with lurid faces. A massive eye adorns the front, while the door of the carriage is a waiting open mouth. Out of a small window, a gnarled hand beckons.

One survivor with 3+ courage may choose to enter the wagon. They may immediately remove one impairment or severe injury and then gain one random disorder.

# 43 - Fresh Kill :
A carpet of skittering bugs point the survivors to a freshly-killed monster. The scavenging insects try to defend their dinner, but the survivors can fight them off to claim their prize.

Each survivor rolls 2d10 and adds their hunt XP to the result. The highest-scoring survivor slays the most vermin and claims the putrid prize for themselves. Gain 1 random basic resource and 1 vermin resource.

# 44 - Opportunists :

While investigating their quarry‘s tracks, the survivors realize they are hunting not one but two monsters. The two sets of tracks soon separate, leaving the event revealer to pick which tracks to follow.

If they follow the larger tracks, continue hunting as normal.

If they follow the smaller tracks, start the showdown immediately. At the start of the showdown the monster suffers 5 wounds. If the survivors are victorious. They gain half the basic resources and half the monster resources (rounded up) from rewards in the aftermath.

# 45 - Jagged Valley :
A valley of sharp white stones opens before the survivors like a toothy maw. As the survivors push and climb through the jagged spires, they find even the shallowest cuts bleed without stopping.

Each survivor takes 1 event damage to the body. Unless a survivor is carrying the Bandages gear, each survivor suffers blood loss, gaining 2 bleeding tokens.

If any survivor has a Pickaxe, they may try to to convince the other survivors to at them investigate a bit more. If they are convinced, roll on the table.

| 1d10 | Survivor carrying a pickaxe - Investigate |
| - | - |
| 1 | The white stones are teeth and the survivors are in a giant mouth! Everyone must spend 1 survival to scamper out of the mouth. Anyone remaining inside is chewed to a pulp. Dead. Do not gain the benefits of Cannibalize. |
| 2 - 5 | The white stone is extremely hard. When the survivors finally break through, a horde of bugs scuttles forth from the holes. Gain 1 Hissing Cockroach vermin resource. The excavation was also time consuming, roll again on the hunt table before moving on the hunt board. |
| 6+ | The survivors find a crack and expand it. Within is a hidden cache! Gain 1 Iron strange resource and 1 Hissing Cockroach vermin resource. The excavation was also time consuming, roll again on the hunt table before moving on the hunt board. |

# 46 - River :
The survivors come to the edge of a river of blood.

Non-insane survivors suffer 1 brain event damage at the sight of it.

The survivors must investigate in order to pick up their quarry's trail again. Each survivor rolls on the table below.

If no survivor successfully finds the monsters trail, roll again on the hunt event table before moving on the hunt board.

| 1d10 | Each survivor - Investigate |
| - | - |
| 1 - 2 | The water is filled with all manner of vile things. Your body is ravaged by parasites. Instantly a massive parasite crams its way down your throat, savaging your insides on the way in. Suffer the Broken Rib severe body injury. |
| 3 - 5 | Your quarry has defecated in the blood. Suffer 1 event damage to a random hit location as you lay retching on the riverbank. |
| 6 - 8 | You realize the blood river is filled with the bloated corpses of unrecognizable monsters. You feel compelled to fish some out! Gain 1 random basic resource. If you are wearing heavy gear, you fall in and swallow blood and soft, bloated monster bits. Reduce your survival to 1.|
| 9+ | You successfully find the monster's trail. |

# 47 - Banquet Trees :
A small copse of trees rises over an otherwise lifeless plain. Enticing red fruit hangs from the branches.

Each survivor must spend 1 survival to resist the temptation to consume the fruit. Insane survivors and survivors with Binge Eating disorders must consume.

If any survivor has a Sickle, they must carve a line across the tree, drawing blood! The fruit instantly sours. End this event.

All survivors who consume the fruit roll on the table below.

| 1d10 | Any survivor - Consume |
| - | - |
| 1 - 5 | The addictive fruit savages your insides. Lose 1 survival and roll again on this table. If you had no survival, suffer 2 damage to a random hit location. |
| 6+ | You belch loudly in satisfaction and walk away. |


# 48 - Death Wager :
The survivors are awoken by a traveler hidden in a deep cloak. The traveler removes his hood to reveal a face whose jaw has been split in two. Two chins curve away from each other, each with its own mouth.

In a dreamlike state, the survivors all understand this entity is known as The Gambler and if they speak they will die. Until this event is completed, only the event revealer can speak. If any other player speaks. Their survivor turns cold and is dead.

The survivors must play The Gambler's game or be trapped with him forever. Each player must roll on the table.

| 1d10 | Each survivor |
| - | - |
| 1 | The Gambler reaches out his hand and scoops up your dice. As it vanishes within a fold of his cloak. you feel all traces of air leave your lungs and are unable to draw another breath. Dead. |
| 2 - 9 | You do not lose. nor do you win. Play again if you wish. If you do, roll again on the table. |
| 10+ | You win. Gain +1 permanent luck. |

# 49 - Pus Fields :

The landscape is dotted with large, swelling mounds oozing pus. The smallest jostle threatens to explode them.

The survivors may **carefully tread** through the mounds or **rush through**.

**Carefully tread:** You make it through without incident, but your progress is slowed. Roll again on the hunt event table before moving on the hunt board.

**Rush through:** Each survivor rolls 1d10 and adds their evasion to the result. On a 7 or less, they detonate one of the pus mounds! They suffer 2 event damage to a random hit location and become stinky for the rest of this lantern year.


# 50 - Gibbet :

A crude iron cage swings from the branches of a massive tree. As the survivors approach, a person calls out to them from the cage, pleading to be freed.

The survivors may pass by and suffer 1 brain event damage as the prisoner weeps and pleads. If the survivors choose to free them, the event revealer rolls on the table.

| 1d10 | Event revealer |
| - | - |
| 1 | The survivors open the cage but all they find inside is a skeleton. All survivors suffer 2 brain event damage. |
| 2 - 7 | The prisoner is thankful and follows the survivors until they rest. When they wake, each survivor loses 1 random resource (if they had any). |
| 8+ | The prisoner is grateful and follows the survivors home after the showdown. The settlement gains +1 population. |

# 51 - Refugees :

The survivors encounter a group of fleeing settlers. If the survivors have innovated Symposium and Language, they may try to crudely communicate. The survivors may try to the refugees a piece of gear or a resource. If they do, archive the offering.

If the survivors gave the refugees an offering, they will stop to tell their mournful tale about the monster that besieged them. Now prepared for what lies ahead, at the start of the showdown, reveal the first 5 Al cards from the quarry‘s Al deck and put them back in any order.

If any survivor has the Leader fighting art, one of the settlers is drawn to their charisma. Gain +1 population.

# 52 - Madflies :

Tiny, persistent insects swarm the survivors, flying into their ears and nostrils. The bugs buzz maddeningly in their heads, growing louder as they nest.

Each survivor rolls on the table.

| 1d10 | Each survivor |
| - | - |
| 1 - 2 | You dive into a nearby marsh. It keeps the flies away, but now you're constantly distracted by the tiny bug corpses you have to hack up. Gain -1 evasion token. |
| 3 - 7 | The buzzing immediately stops and the flies depart. Their rejection makes you feel strangely despondent. Gain +1 insanity. |
| 8+ | Their eggs gestate incredibly quickly and madflies explode forth from your mouth! The experience is gruesome but makes you feel so alive! Suffer the Frenzy brain trauma. If you have the Rageholic or Berzerker fighting art, giving birth to disgusting new life kindles your rage; suffer the Frenzy brain trauma again. |

# 53 - Mask Salesman :

The survivors meet a traveling Mask Salesman on the road. He insists that he has special wares to offer.

Take one copy of each mask from the Mask Maker gear and shuffle them, drawing one at random. This is the mask the salesman is offering. The event revealer rolls 1d10 to determine the price on the table and gains the mask.

| 1d10 | Event revealer |
| - | - |
| 1 - 3 | The salesman silently points at you. Instinctively, you hand him his asking price in exchange for the mask. Archive 1 gear from your gear grid and reduce your survival to 0. |
| 4 - 7 | The salesman slowly gestures for an offering. Instinctively, you hand him his asking price in exchange for the mask. Archive 1 gear from your gear grid. |
| 8+ | As he imparts the mask to you, the Salesman's lips form the sounds of the survivor's name. It (the name) is gone. In exchange for the mask, give your survivor a new name (must be different) and +1 survival for naming your survivor. |

# 54 - Bone Storm :

Mighty windstorms often blow across the open plains. Occasionally they grow so strong that they collect chips of stones and skeletal remains in their shearing depths. The survivors may **brave the storm** or **wait it out**.

**Brave the storm**: Each survivor suffers 1 event damage to 3 random hit locations, gains +1 courage, and archives all fragile gear.

**Wait it out**: Roll twice on the hunt event table before moving on the hunt board.

# 55 - Mudslide :

A flash mudslide sweeps the survivors away. Each survivor must roll on the table.

If any survivor has a whip, they catch it around a pillar-shaped bone and swing to safety; they do not roll.

| 1d10 | Each survivor |
| - | - |
| 1 - 2 | You're swept along, drowning in mud. Suffer 2 event damage to a random hit location and archive all fragile and soluble gear in your gear grid. Roll again on the table. |
| 3 - 7 | You manage to scramble free, but not before taking a serious beating. Suffer 2 event damage to your body and archive all fragile and soluble gear in your gear grid. |
| 8 - 9 | You reach higher ground. You're caked in mud, but otherwise unscathed. |
| 10+ | You remember being pulled under, the sensation of mud forcing itself down your throat, then nothing else. After the mudslide passes, the other survivors find you unharmed. Gain 1 insanity. |

# 56 - Dead Monster :

The survivors find a rotting monster corpse of the same kind they are hunting.

While the corpse is too rotten to be of any use, if any survivor has 3+ understanding, the survivors can cover themselves in the dead monster's spilled blood. The stench will help the survivors sneak up on their quarry. When the showdown starts, the survivors ambush their quarry. If any survivor has the Squeamish disorder, they choose not to soil themselves, do not ambush the monster.

If no survivor has 3+ understanding, the starving survivors feast on the rotting corpse. All survivors lose control of their twisting guts. Each survivor loses 1 survival and no one can consume any items during the rest of this lantern year.

# 57 - Gorm's Laughter :

The rhythmic wail of a Gorm's laughter reaches the survivors, filling them with fear. The Gorm's laughter will follow the survivors, tormenting them until they reach their quarry.

Place 1 token on every remaining hunt board space between the survivors and their quarry. When survivors move into a space with a token, remove the token and all non-deaf survivors suffer 1 brain event damage.

# 58 - Scent on the Wind :

A strong wind blows, bringing with it the scents of distant places and things. The event revealer rolls on the table.

| 1d10 | Event revealer |
| - | - |
| 1 - 5 | The monster catches your scent. It moves one space closer. If this movement starts the showdown, the monster ambushes the survivors |
| 6+ | The survivors smell their quarry‘s foul odor and surge forward. The survivors may skip the next hunt space. If this movement starts the showdown, the survivors ambush the monster. |

# 59 - Signs of Battle :

The survivors come upon the remains of a terrible battle between their quarry and some unknown foe.

The event revealer may choose to investigate. If they do, gain +1 courage and roll on the table. Otherwise, roll again on the hunt event table before moving on the hunt board.

| 1d10 | Event revealer |
| - | - |
| 1 - 2 | The monster springs out of the dark, taking advantage of the distracted survivors. The showdown begins immediately and the monster ambushes the survivors. In addition, during showdown setup, place the event revealer directly in front of the monster. |
| 3 - 7 | The remains are human. Filled with sadness and anger, each survivor suffers 1 brain event damage. If the survivors chose Graves as their death principle they bury the dead, and each survivor gains +1 survival. If they chose Cannibalize as their death principle, gain 1 random basic resource. |
| 8+ | The remains of the tremendous beast prove useful. The event revealer gains 1 random basic resource.

# 60 - Wildfire :

A massive wall of flame confronts the survivors. Beyond it, a path of destruction. The fire has destroyed whatever awaited the survivors.

Archive any hunt event cards in the next 2 hunt spaces. Place two basic hunt event cards in those spaces.

# 61 - Frozen Lightning :

A storm sweeps over the survivors. Deep purple lightning flashes overhead. The bolts freeze in mid-strike and rain down on the survivors as jagged, glowing spikes.

Every survivor must roll 1d10. The lowest scoring survivor (or survivors, in case of ties) becomes a straggler.

A bold crashes beside the straggler, catching them in an explosion of razor sharp crystals. Suffer 1d5 event damage to 2 random hit locations, and archive all fragile gear in the straggler's gear grid.

# 62 - Space Between the Rocks :

The survivors find themselves distracted by a dark crack in the endless sea of stone faces.

If any survivor has a pickaxe, they strike the ground, causing the crack to expand into a gaping hole. Bravely reaching inside the survivor produces a blackened, twisted hunk. Gain one iron strange resource.

Otherwise, every survivor must roll 1d10. The lowest scoring survivor (or survivors, in case of ties] becomes a straggler.

The straggler stoops to gaze into the depths until the other survivors drag them away. Whatever the straggler glimpsed changes them forever. Gain a random disorder.

If all the survivors end up as stragglers, no one ever pulls them away and the entire party is lost. Dead.

# 63 - Feet :

The stone faces ahead are replaced by an expanse of stone feet sprouting from the ground. The survivors walk sole-to-sole with the feet.

If any survivor is insane they are convinced they have reached the underside of the world. Terrified of falling off, they grab hold of the ground for dear life, slowing the party and attracting unwanted attention. Roll once on the hunt event table for each insane survivor before moving on the hunt board. If any of these rolls result in Feet again, ignore and re-roll.

If all survivors are insane, they fall off the underside of the world. The survivors are dead unless the settlement chose Survival of the Fittest. If they did, the survivors bite down hard on the feet. Their teeth grind against stone as they refuse to let go. At once, their teeth shatter, revealing ordinary stone faces beneath. Set all survivors insanity to 0. Each survivor suffers the shattered jaw severe head injury. Do not roll any additional hunt events caused by Feet.

# 64 - Stone Fountain :
A pair of cupped stone hands rise out of the ground. The hands hold clear, cold water that trickles through the stone fingers in a seemingly endless supply.

Any survivor may choose to consume from the fountain. Gain +1 courage and roll on the table.

If no one consumes, roll again on the hunt event table before moving on the hunt board.

| 1d10 | Any survivor |
| - | - |
| 1 - 4 | The water is clear until it passes the survivor's lips, where it turns to blood. You spit out the foul liquid and suffer Monster Level brain event damage. Survivors with the Hemophobia disorder also suffer Monster Level event damage to their body as they vomit violently in disgust. |
| 5 - 8 | It‘s water. Refreshing. |
| 9+ | The water is clean and pure, like none the survivor has ever tasted. Gain +1 survival. |

# 65 - Statue :
Cresting a hill, the survivors find a statue of a man sitting on a throne.

If any survivor has 5+ understanding, the survivors walk past without incident and end this event. Otherwise, every survivor must roll 1d10. The lowest scoring survivor (roll off in case of ties) becomes a straggler.

The straggler is drawn to the statue, touching it gently. In an instant, the straggler is gone and replaced by the man from the throne, who is now flesh and blood. The straggler sits in his place on the throne, stone mouth opens in a silent yell. They are gone forever. Consider them dead, but do not apply the Cannibalize death principle; there is no body.

The man offers his thanks and joins the hunting party without explanation. He is a new survivor with the straggler's gear, 2 random disorders, and 2 hunt XP. Shuffle all weapon specialization cards (including expansions, if any) and draw one. The survivor has 3 ranks of proficiency in that weapon type. They gain +1d10 survival and +1d10 insanity. Give him a name and +1 survival for being named.

If the settlement has Memento Mori, the statue resembles a deceased survivor. Replace the straggler instead with the record sheet of a fallen survivor. Remove  all their severe injuries. Se their hunt XP to 2 (they Age again, as this is a new lifetime). They gain +1d10 survival and +1d10 insanity. You may give them 3 ranks of proficiency in a random weapon type.


# 66 - Forbidden Word :
There is a great rush of air followed by a booming voice. A word is spoken in an unknown language by an inhuman voice. Just for an instant, the survivors understand.

All non-deaf survivors gain +1 understanding and suffer 1 brain event damage.

Insane non-deaf survivors also gain 1 random disorder.

# 67 - Saliva Pools :
The ground ahead is pockmarked with pools filled with a gooey liquid. The pools bubble merrily and stink of digesting meat. As the survivors move between the pools, they are overcome by waves of nausea.

If any survivor has a sickle, they slice a few leaves from a nearby plant and everyone uses them to plug their noses and mask the smell. All survivors gain +1 understanding.

Otherwise, each survivor rolls 1d10. If the result is less than their current survival, they continue on.

If the result is equal or greater than their survival, they vomit into a nearby pool suffering 1 event damage to the body. They witness the pool greedily dissolve their vomit, and gain +1 understanding.

If any survivor has 3+ understanding, they devise a way to use the pools. Each survivor may place one gear in a pool. Archive the item, it is lost. The survivor gains the resources used to craft that gear (if any).

# 68 - A Familiar Face :
If the settlement has not lost any survivors yet, ignore this event and roll again on the hunt event table.

Otherwise, every survivor must roll 1d10. The lowest scoring survivor (or survivors. in case of ties) become a straggler.

While examining the stone faces underfoot, the straggler recognizes the face of a fallen friend.

Bittersweet memories bring the straggler to tears. Reduce their insanity to 0 and they gain +1 understanding.

# 69 - Time Lapse :
The glow of the survivors lanterns burns, forming trails of light behind them.

If the settlement has fought a Phoenix, the group recognizes the phenomenon. All survivors gain +1 understanding.

The event revealer rolls on the table below. If any survivor has an hours ring gear, they may instead select a die result.

| 1d10 | Event revealer |
| - | - |
| 1 - 3 | All survivors suddenly age. All survivors with less than 10 Hunt XP gain +3 Hunt XP. Do not gain the benefits of Age. Your bodies have physically aged, no practical experience has been learned. |
| 4 - 9 | The survivors' minds fill with alien memories. They are no longer just themselves, each walking differently and speaking with strange accents. Give each survivor a new name and +1 survival for being named. Each survivor gains +1 Hunt XP and one random disorder.  If your settlement has Survival of the Fittest and you've already used your once-per-lifetime re-roll, you may use it again. |
| 10+ | The survivors are suddenly standing by their quarry! Start the showdown immediately.

# 70 - Tomb of Excellence :
The survivors find a serene, luxurious tomb, immaculate in its construction and condition. Elegant murals decorate the walls, depicting glorious victories over the very monster the survivors are hunting.

The survivors are baffled by its presence. The event revealer investigates rolls on the table.

| 1d10 | Event revealer - Investigate |
| - | - |
| 1 | The survivors bemoan how weak they are compared to the warriors in the murals. Each survivor loses 1 survival. |
| 2 - 5 | It's nice to see something pretty for once, eh? |
| 7 - 9 | The survivors can’t remember the last time they felt this cheerful and inspired. Each survivor gains +1 courage. |
| 10+ | The doorway of the tomb is framed with a wood that has small hands for grain. Inside, murals depict a hero at the center of a labyrinth that is holding a giant, human-filled fruit above his head, Each survivor gains +1 understanding. If the settlement has Pictographs, the survivors find information about their quarry. At the start of the showdown, they may place the monster's trap at the bottom of the hit location deck. |


# 71 - Found Relic :
Amidst the endless stone faces, the event revealer notices an unusual object on the ground. The event revealer investigates on the table.

If the settlement has Records, add +6 to the result.

| 1d10 | Event revealer - Investigate |
| - | - |
| 1 - 2 | You find a weathered purple stone with a swirling texture. It is shockingly heavy. Mesmerized by the stone, you hide it from the other survivors. Gain the Secretive disorder and -1 movement token as you lug the stone around. |
| 3 - 4 | Useless junk litters the ground. |
| 5 - 9 | You find a stone heart that pulses with warmth as you hold it in your hands. Gain +1 insanity.  |
| 10+ | You find a mask with living lips. If you return to the settlement with the mask, it quickly learns your language and shares its secrets before crumbling to dust. At the start of the next Settlement phase, draw 3 innovations from the innovation deck and add one to your settlement at no cost. If any survivor has a Final Lantern, it begins to flash and vibrate wildly. The mask disintegrates and all survivors suffer 1 brain event damage. |

# 72 - Something to Pass the Time :
If the survivors have not innovated Symposium, ignore this event and roll again on the hunt event table before moving on the hunt board.

Otherwise, the event revealer suggests a word game to lighten the mood as the survivors trudge forward. The event revealer rolls on the table. If the settlement has Song of the Brave, add +4 to your roll.

| 1d10 | Event revealer |
| - | - |
| 1 - 2 | The survivors‘ boisterous voices attract their quarry and the monster ambushes them. |
| 3 - 7 | No one else is in the mood for a game. The event revealer loses 1 courage. |
| 8 - 9 | The survivors play the simple but entertaining game, gaining +1 courage each. |
| 10+ | The game is astonishingly fun, and the survivors’ spirits soar with their booming laughter. Roll 1d1O for each survivor. If the result is 6+, that survivor can cure 1 disorder or gain 1 survival.

# 73 - Golden Ember :
The way forward is blocked by dense, gold-flecked smoke. If the survivors have a Final Lantern, it guides them through the smoke without any trouble. End this event.

The survivors may push ahead and brave the smoke. If they do, each survivor gains +1 courage and the event revealer rolls on the board below.

If they don’t brave the smoke, they take the long way around; roll again on the hunt event table before moving on the hunt board.

| 1d10 | Event revealer |
| - | - |
| 1 - 5 | The smoke overwhelms the survivors. As they run through the glittering smog, each survivor suffers 1 event damage to the head and chest locations when they finally reach the haze's end, they silently walk on. No one is comfortable sharing the terrible things they glimpsed. Each survivor gains 1 random disorder. |
| 6+ | The survivors grasp each other‘s hands, form a line, and traverse the smoke. They breathe through rags and takes turns peeking out for orientation. Somewhere, deeper than the smoke, they stumble across a crater. They may ignore it and escape the smoke (ending this event) or explore the crater. |

The survivors carefully descend the crater, finding the ruins of a settlement surrounded by a ring of skulls. The settlement is absolutely demolished and almost everything has been ground to powder from some tremendous force. The skulls face away from the ruins and in each gaping mouth, a golden ember spews smoke.

The sight is haunting. Each survivor suffers 1d10 brain event damage and gains 1 random disorder. The survivors gingerly sift through the ruins. Nominate a survivor to investigate and roll on the table.

| 1d10 | Nominated survivor - Investigate |
| - | - |
| 1 | Against all reason, you feel compelled to reach into a skull and take an ember. Your flash sizzles, blinding you with pain yet you cannot or stop yourself from reaching for your own mouth. If you have a broken jaw, you're thwarted. You drop the golden ember from your burning hand. Suffer the dismembered arm severe injury. Shaken, the survivors flee the smoke. Otherwise, your jaw clamps shut around the ember, the flesh of your mouth melts shut, muffling your cries of pain. Smoke pours from your eyes, nose, and ears. If the settlement has Survival of the Fittest, the urge to live wins out and you savagely break open your own jaw. Suffer the broken jaw severe head injury. The ember tumbles to the ground, it’s sputtering hiss ringing in your ears as all survivors flee in horror. Otherwise, your smoking, melting corpse marches about robotically into the ring of skulls, laying itself in the place of the disturbed skull. The putrid smell of your burning brain suffuses the smoke in the air. The site is so disturbing that all other survivors flee, never to speak of what happened. |
| 2 - 4 | You sense the extreme danger from the golden embers and linger just long enough to grab a small trinket. Gain 1 Broken Lantern basic resource. |
| 5 - 8 | You notice that the settlement is strewn with lanterns, all extinguished and smashed nearly beyond recognition. The survivors resolve this will never happen to them. If any survivor returns to the settlement this year, gain +2 to endeavors. |
| 9+ | Amidst the ruins, you find only one unbroken object. A Twilight Sword. It rests, respectfully laid across a mostly undamaged survivor's skeleton. If the settlement does not have a Twilight Sword, and no survivor in the group has the Honorable disorder, you may nominate a survivor to gain the Twilight Sword rare gear. If you do and the Hooded Knight story event is not on the timeline, add it to the timeline 2 years from now. |


# 74 - Antler-Gouged :
A great battle of dominance between two enormous antelope has left the stone-faced ground gouged with criss-crossing scars. Each survivor must roll on the table below.

| 1d10 | Each survivor |
| - | - |
| 1 - 2 | The survivor trips over the jagged ground, tearing their flesh. Gain the torn muscle severe leg injury. |
| 3 - 8 | The survivor treads carefully over the destroyed faces. |
| 9+ | The survivor finds a loosened stone. Gain a Founding Stone gear. |

# 75 - Oops! :
Stumbling through the darkness, the event revealer crashes to the ground, crushing their lantern beneath them.

The living light inside the lantern becomes agitated by the survivor's clumsiness and burns a piece of gear.

The event revealer must archive 1 gear card of their choice from their gear grid.

# 76 - Dream the Way :
The survivors have vivid, fevered visions of what lies ahead. All at once, they awake with a start and frantically compare the horrors they foresaw.

Each survivor rolls 1d10. If any survivor is a savior, their powerful dream envelops the group; each survivor may select whatever roll result they want. If any survivor roll result is the same, those survivors discover they had the very same dream. Each gain +1 insanity.

If any duplicate rolls are also 10s, the event revealer may re-roll and one roll result this hunt phase (any one die roll, not just their roll).

# 77 - Sinkhole :
A gaping sinkhole suddenly opens under the survivors, revealing a swirling black pool of ichor below them.

Each survivor must roll 1d10. The lowest scoring survivor (or survivors, in case of ties) become a straggler.

Each straggler rolls on the table below.

If any other survivor has a whip, they throw a line to the straggler. Each straggler adds +4 to their roll.

| 1d10 | Straggler |
| - | - |
| 1 |You are swallowed by the sinkhole. You are pulled to the surface moments later, but completely naked, your gear hopelessly lost in the miasma. Archive all gear in your gear grid. |
| 2 - 4 | The other survivors haul you up, but not before something is sucked below the surface. Archive 1 gear of your choice from the gear grid. |
| 5 - 9 | You are dragged to safety, terrified but unharmed. |
| 10+ | After a monumental effort, the other survivors pull you free. Someone else clinging tightly to your feet (+1 population)! As they retreat to your settlement, each survivor suffers 1 brain event damage. |

# 78 - Dead Great Game Hunter :

The survivors find a corpse dressed in brightly-colored clothing, clutching something to its chest.

The event revealer may choose to investigate. If they do, they gain +1 courage and roll on the table. If any survivor has a whip, they lash the corpse from afar; add +4 to your roll.

Otherwise, roll again on the hunt event table before moving on the hunt board.

| 1d10 | Event revealer |
| - | - |
| 1 | He was holding an explosive! Moving it causes it to detonate, it blows off the event revealer's hand. Gain the dismembered arm severe arm injury. |
| 2 - 4 | He was holding an explosive. It explodes but the event revealer pulls away before disaster strikes. Gain -1 accuracy token. |
| 5 - 9 | His hands are empty. If your settlement chose Cannibalize as their death principle. gain 1 random basic resource. If your settlement chose Graves as their death principle. the event revealer gains +1 understanding. |
| 10+ | His hands contain a jeweled bottle filled with a chartreuse liquid. The event revealer gains 1 Frenzy Drink gear. |

# 79 - Dying Small Prospector :
If your settlement already has a Portcullis Key, the prospector is gone. Roll again on the hunt event table before moving on the hunt board.

The survivors discover a body slumped against a large stone face. Moving closer, they find a small, dying prospector riddled with arrows. As they approach, he growls a warning, threatening them with a huge stone shard. When he sees that they are not evil monsters, he calms down and gives them a key. With his dying breath he says: "This is the key to the portcullis. Without it, you will never get through."

Record the Portcullis Key in the settlement record sheet notes.

If the settlement has Graves, the event revealer builds a small monument of broken stone noses and gains +1 understanding.

# 80 - Lovelorn Rock  :

The survivors pass a ring of stones with an unassuming boulder at the center.

Every survivor must roll 1d10. The lowest scoring survivor (randomize in case of ties) becomes a straggler.

The straggler hefts the boulder (it's heavy!) and promises to carry it everywhere, forever in love. This survivor must always leave one gear space empty, as it contains their beloved rock. Record this on your survivor sheet.

The rock can be lost and archived like normal gear. If it is lost, the survivor regains the space in their gear grid, mourning the loss of their beloved.

# 81 - Field of Arms :

The survivors carefully trade along the back of a massive, sleeping monster. Instead of fur, it has elongated arms, several of them twitching to whatever dream the great beast is in the midst of. The survivors don’t disturb the monsters deep sleep and cross without issue.

If any survivor has a sickle, they gingerly remove some tough skin from one of the arms and gain one Hide basic resource.

# 82 - Consuming Grass :
Vibrant green grass grows in patches ahead of the survivors. Closer inspection of the delicate leaves reveals them to be as sharp as any blade.

Every survivor must roll 1d10. The lowest scoring survivor (randomize in case of ties) becomes a straggler.

As the survivors carefully pick their way past the verdant hazards, the straggler stumbles into the brush, and rolls on the table.

If any Survivor has a whip, a hasty tether is made. Add +4 to your roll.

| 1d10 | Straggler |
| - | - |
| 1 | The survivor lands in the grass patch. Quickly climbing to their feet, they realize it's too late. The parts of their body that touched the ground are sprouting with sharp grass blades. Any attempt to remove the grass spreads it further over the survivor's body. During the showdown, you are never a threat (ignore any effect that would make you a threat, including the White Lion's Sniff). At the end of the showdown, this survivor's body blossoms into a whorl of immaculate green grass. Dead. |
| 2 - 9 | The survivor falls, but manages to interpose something between the grass and their bare skin. Either archive 1 gear of your choice to protect yourself, or keep it and treat this result as if you rolled a 1 on this table. |
| 10+ | The survivor stops their fall before it's too late.

# 83 - Flesh Monolith :

The survivors approach a 5-sided monolith made of flesh that stretches into the darkness overhead. Limbs and faces of humans and creatures alike protrude from all sides.

The event revealer gains +1 courage and makes an investigation roll on the table.

| 1d10 | Event revealer - Investigate |
| - | - |
| 1 | The limbs of the monolith spring to life, grabbing you and tearing you limb from limb, joining the parts of your body to the monolith with maddening efficiency as you are ripped apart in a shower of gore. Dead. The horrible sight causes all all other survivors to suffer 3 brain event damage and they all gain Post Traumatic Stress disorder. If the settlement has Survival of the Fittest, you fight the monolith! The horrible edifice tears your arm off, but you bite one of its appendages in return, severing it! Suffer the dismembered arm severe injury and gain +1 permanent strength. All other survivors stand in awe and gain +3 insanity and +1 courage. |
| 2 - 4 | Hands and tentacles grasp at you. Spend 1 survival or treat this result as if you rolled a 1 on this table. |
| 5 - 9 | All survivors are driven back with horror, suffering Monster Level brain event damage. |
| 10+ | As the survivors draw near, they discover that this flesh monolith is actually an enormous pile of survivor's corpses, many with badly broken limbs. The pile is covered in chunks of strange, sweet-smelling viscera. If the settlement has Graves, you ascend the heap and find 3 barely living survivors, the their bodies devoid os nourishment and their eyes too sensitive for lantern light. If you return to the settlement, +3 population, These new survivors cannot depart for 2 lantern years as they need time to recover (note this on the timeline). Otherwise, all you manage to harvest from the pile emaciated corpses is 2 Organ basic resources. Their skin is too soft and bones too brittle to be of any use. |

# 84 - Scribe's Book :

A huge, ornately-bound book lays open before the survivors.

If the settlement has innovated Pictographs, any survivor with 3+ courage may write their name in the book. If any survivor has 3+ courage and is Insane, they must write.

Each survivor that writes their name in the book rolls on the table below, adding their understanding to the result.

If no one writes in the book, roll again on the hunt event table before moving on the hunt board.

| 1d10 | Any survivor |
| - | - |
| 1 - 4 | As you finish writing your survivor's name you know that you did something terribly wrong. Your survivor vanishes from history. They are dead. Archive their gear. |
| 5 - 8 | Nothing happens. |
| 9 - 10 | As you write your name, you feel restored. Heal all injury levels and lost armor points. Gain +2 survival. |
| 11 - 14 | You feel assured that as long as your name is in the book nothing bad can happen to you. Choose to gain two of the following: +2 courage. +2 understanding, or +2 survival. |
| 15+ | The book proves the undeniable mark you leave on the world. You feel more substantial. Gain +1 understanding, +1 courage, +1 survival, +1 permanent speed, and +1 permanent strength. |

# 85 - Test of Courage :

Lava flows from the eyes of a huge, grimacing stone face. Its gritted teeth appear to hold a worn sword.

If there are any survivors with 6+ courage, choose one to brave the lava and gain the Adventure Sword rare gear. If no one can test their mettle, the group moves on with a feeling of inadequacy.

If the settlement already has an Adventure Sword and Storytelling, the survivors share stories of their bravery; each survivor gains +1 survival. If the settlement also has Saga, the stories are exceptionally moving; each survivor also gains +1 courage.


# 86 - Putrid Tunnels :

The survivors smell it long before they see it, a series of cave mouths that emit noxious odors.

Each survivor rolls 1d10. On a 9+ they enter one of the tunnels and discover a wretched group of diseased survivors living in filth.

All survivors who enter the cave catch their foul, rotting disease. They have leprosy; reduce all damages suffered by 1 (to a minimum of one). Suffer -2 to severe injury rolls. Record this impairment.


# 87 - Weeping Faces :

Water flows from the eyes of the surrounding stone faces, gathering in a small pool.

Any survivor may choose to consume from the pool and roll on the table below. If the survivor has 3+ understanding, add +2 to the result.

Insane survivors begin to weep uncontrollably. If any survivor is insane, roll again on the hunt event table before moving on the hunt board.

| 1d10 | Any survivor - Consume |
| - | - |
| 1 - 3 | The water is salty and sour. You can't help thinking about what you might be drinking. Suffer 1 brain event damage. |
| 4 - 6 | Refreshing. Nothing happens. |
| 7 - 8 | The water is cleaner than most. You may heal up to 2 injury levels on any one hit location. |
| 9+ | The water is invigorating. Gain +1 speed token. |


# 88 - The Sword and the Statue :

A statue, twice as tall as any man, sits before a great anvil with a hammer in each of its six hands. Transfixed, the survivors watch the statue beat a red-hot sword that lies across the anvil.

Each survivor, starting with the event revealer and moving clockwise, may make one attempt to grab the sword from the anvil.

If a survivor makes an attempt, they gain +1 courage and roll on the table, adding their Hunt XP to the result.

| 1d10 | Any survivor |
| - | - |
| 1 - 2 | As you get close to the anvil, the statue grabs hold of the sword and plunges it into your body. There is a sharp hiss as the hot metal cools in your blood. You are dead. |
| 3 - 8 | You make a quick grab for the sword, but not quick enough. Suffer the dismembered severe arm injury. |
| 9 - 13 | You may not be quick enough to grab the sword, but at least you're not foolish enough to die trying. |
| 14+ | Your speed is legendary. Gain the Muramasa rare gear. End this event. |

# 89 - Cleaner Birds :

Tiny ragged birds with needle-thin beaks fly overhead.

Every survivor must roll 1d10. The lowest-scoring survivor (roll off in case of ties) becomes a straggler.

The birds swarm the straggler. One bird forces its way into the straggler‘s mouth and down their throat. The straggler vomits up the well-fed bird, their insides scrambled and scarred. Gain +1 permanent luck and -1 permanent speed.

If any survivor has a whip, they crack it and fell the offensive creature. Gain 1 random basic resource.

# 90 - Light on the Horizon :

The survivors hear a screeching howl, followed by a crash, and finally an explosion of multicolored light on the horizon. The unnatural light illuminates the survivors’ way.

If at least one survivor is sane, they follow the light. You may reroll the next result on the hunt event table.

If all survivors are insane, they turn away from the light, walking into the darkness. Move the survivors 2 spaces away from their quarry on the hunt board.

Regardless of what the survivors do, the light reaches the settlement. Add the Lights in the Sky settlement event to the timeline next year.

# 91 - The Beginning :

The survivors stumble upon the scene of their settlement’s first hunt. Whether they've seen it themselves or heard of it through stories, they immediately recognize it.

Seeing the spot of their settlement's first triumph is electrifying. Each survivor gains +1 survival.

If the settlement has Saga, each survivor gains +1 courage.

If the settlement has Storytelling, each survivor gains +1 understanding.

# 92 - Failed Start :

The survivors find the tattered remains of four humans. Clad in loincloths and clutching stone shards, they bear the distinct marks of White Lion claws.

Each survivor suffers 1 brain event damage and gains 1 Founding Stone starting gear.

If any survivor has 3+ understanding, also they gain +1d10 insanity.

# 93 - Lost Survivor :

In a hollow between two identical rocks, you find a corpse with fabulous hair, clutching a book to its chest.

If the settlement innovated Pictographs, the event revealer may read from the book and roll on the table. Otherwise, the survivors move on, deeply confused. The book tells the tragic tale of two survivors, whom found a love they could never have.

| 1d10 | Each survivor |
| - | - |
| 1 - 3 | Page 3 - Dual Nature. The lovers‘ settlement is destroyed, and they are stolen away. The event revealer feels conflicted. After the showdown, they leave the settlement forever in search of something. |
| 4 - 6 | Page 6 - The man spent years undergoing countless tortures for the sake of his beloved. Each torment only served to strengthen the man's resolve. Gain +1 permanent speed and the Anxiety and Traumatized disorders. |
| 7+ | Page 9 - The man escaped, yet never returned. He knew that if he did, it would mean the death of his beloved. He decided it was best for him to fade into memory to protect the few smiles his existence had inspired. Gain +1 courage and the Tough and Last Man Standing fighting arts. |

# 94 - Sickening Mess :

The survivors come upon a patch of badly-damaged ground. The stone faces are cracked and debris is strewn everywhere. Piles of toxic-smelling dung and half-digested viscera litter the area. The survivors approach, but survivors with the Squeamish disorder refuse to go any closer..

Insane survivors consume what they find and make themselves sick; they gain -1 strength token.

Sane survivors stop to investigate the mess; they gain +1 courage, rolls 1d10, and adds their understanding.

| 1d10 | Each sane survivor - Investigate |
| - | - |
| 1 - 2 | You become lightheaded sifting through the piles of dung; uncontrollable gagging ensues. Gain -1 strength token. |
| 3 - 8 | Heaving, eyes filling with tears, your sickening hunt pays off. Gain 1 random basic resource and suffer 1 event damage to a random hit location. |
| 9+ | Your intuition pays off! Gain 1 random basic resource. |


# 95 - Grim and Frostbitten :

There is a dead stillness in the air. The atmosphere becomes thick with worry and the survivors carry on nervously. Soon, a bitter, evil cold sets in and there is no shelter. The survivors huddle together for warmth, shivering loudly.

Unless a survivor has armor gear at each of their hit locations, they lose quarry Monster Level survival.


# 96 - Cloaked Stranger :

A cloaked form steps out from a patch of darkness ahead of the survivors. It’s ill-fitting garment shifts atop its form and its trudging leaves dark, black puddles in the mouths of the rain-slick stone on the ground.
The event revealer approaches the stranger and rolls on the table.

| 1d10 | Event revealer |
| - | - |
| 1 - 2 | The strangers approach fills your ears with painful static. There is a flash of light and a loud crack. You fall to the ground, holding your bloody chest in pain. Suffer the ruptured spleen severe body injury. All non-deaf survivors suffer 2 brain event damage. |
| 3 - 4 | The stranger ripples in and out of focus as you approach. When you reach them, they are gone, their form impossible to remember. Gain 1 bleeding token and forget 1 fighting art of your choice. |
| 5 - 6 | As you near the stranger, you realize that they’re floating in the air! You barely make out a dry rasping coming from the form that seems to resemble... laughter? After the stranger departs, you return to the group, face bone-white, refusing to speak of what you saw. Suffer 3 brain event damage. |
| 7 - 8 | The stranger raises its arms and moves to meet your approach. From beneath the folds of its cloak, you can just make out a hint of violet, scaly hide. A claw extends from the sleeve and gently strokes your cheek, leaving a layer of viscous liquid behind. Wiping off the sludge, you find the bizarre experience strangely touching. You gain +1 survival, +1 understanding. |
| 9+ | The stranger orders you to organize a test of strength among the survivors. Each survivor must roll 1d10 (roll off in case of ties). The highest scoring survivor wins the melee. The stranger marks their face with a glistening claw, granting them the Iron Will ability: You cannot be knocked down. Reduce all knockback you suffer to knockback 1. Record this ability. All survivors lose 4 survival from the exhausting fight. |


# 97 - Living Stone :

The ground suddenly shifts and rises sharply into the air! The survivors find themselves clutching the back of a giant creature that lay sleeping beneath their feet.

The survivors hold on for dear life, absolutely terrified. Nominate a  survivor to climb toward the top of the giant and see what lies ahead. They roll on the table below.

| 1d10 | Elected survivor |
| - | - |
| 1 | Your motion irritates the giant, sending the survivors flying off! All survivors suffer 1d5 event damage to a random hit location. Archive all fragile year. |
| 2 - 7 | You reach the top and have a perfect vantage point to see the world, if only there were enough light to see it. Set your insanity to 0. The giant carries you exactly where you’re going! Start the showdown immediately. |
| 8+ | You discover a bizarre creature with an inverted face diligently carving upon the giant's back. It discards damaged stone faces as it works. Each survivor catches a fragment, gaining 1 Founding Stone starting gear. Eventually, the giant settles down into the ground again. You dismount and discover your quarry! Ambush the monster! |


# 98 - Bloody Eyes :

The survivors find a trail of blood. It pools in the eye sockets of the stone-faced ground. The blood is still warm.

The event revealer may investigate and roll 1d10, or ignore the trail and end this event.

| 1d10 | Event revealer - Investigate |
| - | - |
| 1 - 3 | One of your own is slumped at the end of the trail, bleeding terribly. Choose a random survivor, they gain 3 bleeding tokens. All survivors suffer 3 brain event damage. |
| 4 - 6 | The trail leads to a huge box. Trapped inside is a young red-haired survivor. Gain +1 population. The carvings adorning the box are haunting. All survivors suffer 3 brain event damage |
| 7+ | The trail leads to your quarry! The monster is wounded from a previous battle. Start the showdown immediately. At the start of the showdown, the monster suffers 1 wound. |


# 99 - Portcullis :

The survivors approach a massive portcullis standing in the darkness. It is not attached to anything and does not bar their way.

They may choose to walk around it. If they do, roll again on the hunt event table before moving to the next hunt space on the hunt board.

If the settlement has the Portcullis Key, they may erase it from the settlement and use it. Each survivor gains +1 courage. The portcullis creaks open, and the survivors step through. A dank gloom awaits the survivors inside.

| 1d10 | Event revealer |
| - | - |
| 1 | The portcullis suddenly slams shut behind them and the lights of their lanterns begin to dim. The last thing the survivors see is the grimace of fear on each other's faces as the dark closes in. The survivors are dead. |
| 2+ | At their feet lies an ornate crucible with a void in the shape of a mighty weapon. The survivors gain the Perfect Crucible strange resource. If they have a Blacksmith in their settlement, they may now craft a Perfect Slayer. |

# 100 - The Finale :

An enormous, metallic sound rings out from a the distance. All survivors are electrified with dread; they suffer 2 brain event damage.

The survivors may choose to follow the sound. Otherwise they panic and retreat in the opposite direction; end this event and move back 2 hunt spaces on the hunt board and continue hunting as normal.

If the survivors investigate, each survivor gains +1 courage and gingerly approaches the origin of the sound. As they travel, they pass the shattered corpses of strange beasts.

If any survivor has 3+ understanding, they follow the trail of corpses. Otherwise, harvest 3 random basic resources from the corpses and end the event.

If the survivors follow the corpses, they arrive at a massive anvil. The survivors see a giant, one-eyed knight, its charcoal-colored armor reflecting their lantern light. The event revealer must roll on the table.

| 1d10 | Event revealer |
| - | - |
| 1 | The knight stares at the survivors. All survivors with less than 3 courage are struck dead. All other survivors flee in horror. |
| 2 - 8 | The knight approaches the survivors. In an instant, it chops off a random survivor's ear; they gain 1 bleeding token. Then, it strikes the anvil, blinding the survivors with a churning wall of sound. When they open their eyes, the knight is gone and the Steel Sword and Steel Shield rare gear rest in its place. The group divides the gear between them. |
| 9+ | The knight smashes the object in its hands upon the anvil. When the molten orange object cools, it sets into a massive lion-faced hammer. The knight places it in the hands of the survivor with the highest courage (roll off in case of ties). They gain the Thunder Maul rare gear. A current of electricity runs through their body, joining weapon and survivor forever. |

# keep this line here or the regex breaks
